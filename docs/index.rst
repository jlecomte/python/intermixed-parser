Intermixed Argument Parser
==========================

Intermixed argparser, aka imxdparser, and based only on argparse, is an argument parser that allows the unordered, and intermixed, optional and positional arguments.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   imxdparser.rst

.. toctree::
   :maxdepth: 1
   :caption: Miscellaneous

   license.rst

Indices and tables
==================

* :ref:`genindex`
