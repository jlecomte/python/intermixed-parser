[![license](https://img.shields.io/badge/license-MIT-brightgreen)](https://spdx.org/licenses/MIT.html)
[![documentation](https://img.shields.io/badge/documentation-html-informational)](https://imxdparser.docs.cappysan.dev)
[![pipelines](https://gitlab.com/cappysan/imxdparser/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/cappysan/imxdparser/pipelines)
[![coverage](https://gitlab.com/cappysan/imxdparser/badges/master/coverage.svg)](https://imxdparser.docs.cappysan.dev/coverage/index.html)

# Intermixed Argument Parser

Intermixed argparser, aka imxdparser, and based only on argparse, is an argument parser that allows the unordered, and intermixed, optional and positional arguments.


## Installation

You can install the latest version from PyPI package repository.

~~~bash
python3 -mpip install -U imxdparser
~~~


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Documentation: [https://imxdparser.docs.cappysan.dev](https://imxdparser.docs.cappysan.dev)
  * Website: [https://gitlab.com/cappysan/imxdparser](https://gitlab.com/cappysan/imxdparser)
  * PyPi: [https://pypi.org/project/imxdparser](https://pypi.org/project/imxdparser)
