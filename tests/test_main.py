import argparse

from pytest import mark, raises

from imxdparser import ChildParser, MainParser


@mark.parametrize(
    "cmdline, result",
    [
        # fmt: off
        ("", 2),
        ("-c", 2),
        ("-c one", {'cfgfile': 'one', 'prog': None}),
        ("-c cfg", {'cfgfile': 'cfg', 'prog': None}),
        ("one", 2),
        ("one file", {'cfgfile': None, 'file': 'file', 'prog': 'one'}),
        ("-c cfg one file", {'prog': 'one', 'cfgfile': "cfg", 'file': 'file'}),
        ("one -c cfg file", {'prog': 'one', 'cfgfile': "cfg", 'file': 'file'}),
        ("one file -c cfg", {'prog': 'one', 'cfgfile': "cfg", 'file': 'file'}),
    ],
)
def test_simple(cmdline, result):
    main_parser = MainParser()
    main_parser.add_argument("-c", "--cfgfile")
    main_parser.attach()
    subparser = ChildParser(main_parser, "one")
    subparser.add_argument("file")
    subparser.attach()

    argv = cmdline.split(" ")
    try:
        args = vars(main_parser.parse_args(argv))
        args.pop("func", None)
        assert args == result
    except argparse.ArgumentError:
        assert result == -1
    except SystemExit as err:
        assert result == err.code


def test_exceptions():
    main_parser = MainParser()
    main_parser.add_argument("-c", "--cfgfile")
    main_parser.attach()
    with raises(RuntimeError, match="Cannot add arguments to a parser that has been finalized"):
        main_parser.add_argument("-b")

    subparser = ChildParser(main_parser, "one")
    subparser.add_argument("file")
    subparser.attach()
