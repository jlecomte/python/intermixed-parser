#!/usr/bin/env python3
import sys

sys.path.append(".")

# pylint: disable=wrong-import-position
from imxdparser import ChildParser, MainParser


def parser_error(parser, argv, *_args):
    # Reparse with an additional parameter to force an error
    argv += [""]
    parser.parse_args(argv)


def main(argv=None):
    #
    # application
    # ├── foobar
    # │   ├── duplicate
    # │   └── merge
    # └── thingy
    #     ├── press
    #     └── steam
    #
    if argv is None:
        argv = sys.argv[1:]

    def error(*_args):
        parser_error(main_parser, argv)

    main_parser = MainParser("application")
    main_parser.add_argument("-c", "--cfgfile")
    main_parser.add_argument("--version", action="version", version="1.2.3")
    main_parser.attach()
    main_parser.set_defaults(func=error)

    # --------------------------------------------------------------------------
    foobar = ChildParser(main_parser, "foobar")
    foobar.attach()
    foobar.set_defaults(func=error)

    # --------------------------------------------------------------------------
    def print_foobar1(*_args):
        print("duplicate")

    foobar1 = ChildParser(foobar, "duplicate")
    foobar1.add_argument("src", help="Lorem ipsum sit dolor amet")
    foobar1.add_argument("dst", help="Lorem ipsum sit dolor amet")
    foobar1.attach()
    foobar1.set_defaults(func=print_foobar1)

    # --------------------------------------------------------------------------
    def print_foobar2(*_args):
        print("merge")

    foobar2 = ChildParser(foobar, "merge")
    foobar2.add_argument("src", help="Lorem ipsum sit dolor amet")
    foobar2.attach()
    foobar2.set_defaults(func=print_foobar2)

    thingy = ChildParser(main_parser, "thingy")
    thingy.attach()
    thingy.set_defaults(func=error)

    # --------------------------------------------------------------------------
    def print_thingy1(*_args):
        print("steam")

    thingy1 = ChildParser(thingy, "steam")
    thingy1.attach()
    thingy1.set_defaults(func=print_thingy1)

    # --------------------------------------------------------------------------
    def print_thingy2(*_args):
        print("press")

    thingy2 = ChildParser(thingy, "press")
    thingy2.attach()
    thingy2.set_defaults(func=print_thingy2)

    args = vars(main_parser.parse_args(argv))
    if args.get("func"):
        args["func"](args)
    print(args)


if __name__ == "__main__":
    main()
